import React from 'react';

import Header from  '../components/Header'
import MoviesContainer from '../components/MoviesContainer'

class Home extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <MoviesContainer />
            </React.Fragment>
        )
    }
}

export default Home;