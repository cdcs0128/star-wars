import React from 'react';

import Header from  '../components/Header'
import CharactersContainer from '../components/CharactersContainer'

class MoviesDetail extends React.Component {

    state = {
        loading: true,
        error: null,
        data: {
            characters: [],
        }
    }

    componentDidMount() {
        this.fetchCharacters();
    }

    fetchCharacters = async e => {
        this.setState({loading: true, error: null})

        try {
            const movieId = this.props.match.params.movieId;
            const response = await fetch(`https://swapi.co/api/films/${movieId}`);
            const data = await response.json();
            this.setState({ loading: false, data: data})
            // console.log('asdasd', data);
        } catch (error) {
            this.setState({ loading: false, error: error})
        }
    }


    render() {
        return (
            <React.Fragment>
                <Header />
                <CharactersContainer characters={this.state.data.characters}/>
            </React.Fragment>
        )
        // return console.log('sdsf', data)
    }
}

export default MoviesDetail;