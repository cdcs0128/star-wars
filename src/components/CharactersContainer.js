import React from 'react';

import CharacterDetail from './CharacterDetail'
import './styles/CharactersContainer.css'

function CharactersContainer (props) {

    const characters = props.characters;
    console.log(characters)

    return (
        <div className="container-list-characters">
            <table className="Characters-container">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Height</th>
                        <th>Mass</th>
                        <th>Hair color</th>
                        <th>Skin color</th>
                        <th>Eye color</th>
                        <th>Birth year</th>
                        <th>gender</th>
                    </tr>
                    {characters.map( character => (
                        <CharacterDetail key={character} endpoint={character}/>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default CharactersContainer