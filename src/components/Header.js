import React from 'react'

import { Link } from 'react-router-dom';
import logo from '../images/sw_logo.png'
import './styles/Header.css'

class Header extends React.Component {
    render () {
        return (
            <Link to={'/'}>
                <div className="header">
                    <img src={logo} alt="Start-wars" />
                </div>
            </Link>
        )
    }
}

export default Header