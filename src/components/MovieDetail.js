import React from 'react'

import './styles/MoviesContainer.css'


class MovieDetail extends React.Component {

    handleHover = e => {
       
        console.log("Button was clicked");
    };

    render() {

        const movie = this.props.movie;
        return (
            <React.Fragment>
                <div onMouseEnter={this.handleHover} className="crawl-text">
                    <div className="crawl-text__box">
                        <p className="crawl-title">{movie.title}</p>
                        <p className="crawl-text-paragraph">{movie.opening_crawl}</p>
                    </div>
                </div>
                <div className="content__info-movie">
                    <h2 className="info-movie__title">Title: {movie.title}</h2>
                    <p className="info-movie__director">Director: {movie.director}</p>
                    <p className="info-movie__producer">Producers: {movie.producer}</p>
                    <p className="info-movie__release-date">Released: {movie.release_date}</p>
                </div>
            </React.Fragment>
        );
    }

}

export default MovieDetail;