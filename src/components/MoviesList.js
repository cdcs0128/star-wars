import React from 'react'
import { Link } from 'react-router-dom'

import './styles/MoviesList.css'
import MovieDetail from './MovieDetail'

function MoviesList (props) {

    const movies = props.movies;

    return (
        <ul className="List-movies row">
            {movies.map( movie => (
                <li className="item-movie" key={movie.episode_id}>
                    <Link to={`/${movie.episode_id}`}>
                        <MovieDetail movie={movie}></MovieDetail>
                    </Link>
                </li>
            ))}
        </ul>
    )

}

export default MoviesList;