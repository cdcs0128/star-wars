import React from 'react'

import './styles/MoviesContainer.css'
import MoviesList from './MoviesList';


class MoviesContainer extends React.Component {

    state = {
        loading: true,
        error: null,
        data: {
            results: [],
        },
    };


    componentDidMount() {
        // console.log('1..');
        this.fetchMovies();
        // this.intervalId = setInterval(this.fetchMovies, 5000);
    }

    fetchMovies = async () => {

        this.setState({ loading: true, error: null})

        try {
            const response = await fetch('https://swapi.co/api/films/');
            const data = await response.json();
            this.setState({ loading: false, data: data})
            console.log(data);
        } catch (error) {
            this.setState({ loading: false, error: error})
        }


    }

    render() {
        
        const movies = this.state.data;

        

        // if(this.state.loading === true) {
        //     return console.log('Loading....');
        // }

        // if(this.state.error) {
        //     return console.log('Error');
        // }

        return (
            <React.Fragment>
                <div className="Movies__List">
                    <div className="moviesList__container">
                        <h2 className="Title-section"> Movies</h2>
                        <MoviesList  movies={movies.results}/>
                        {/* <ul>{movies.results.map( elem =>(
                            <li key={elem.episode_id}>{elem.title}</li>
                        ))}</ul> */}
                        {/* <li></li> */}
                    </div>
                </div>
            </React.Fragment>
        );
    }

}

export default MoviesContainer;