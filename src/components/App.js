import React from 'react';
import { BrowserRouter, Switch , Route } from 'react-router-dom'

// import Layout from './Layout'
import Home from '../pages/Home';
import MoviesDetail from '../pages/MoviesDetail'

function App() {
    return (
        <BrowserRouter>
            {/* <Layout> */}
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/:movieId" component={MoviesDetail} />
                </Switch>
            {/* </Layout> */}
        </BrowserRouter>
    );
}

export default App;