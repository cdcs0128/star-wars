import React from 'react'

import './styles/CharacterDetail.css'

class CharacterDetail extends React.Component {

    state = {
        nextCharacter: 0,
        data: {
            name: undefined,
            height: undefined,
            mass: undefined,
            hair_color: undefined,
            skin_color: undefined,
            eye_color: undefined,
            birth_year: undefined,
            gender: undefined
        },
        loading: true,
        error: null
    }       

    componentDidMount() {
        this.fetchCharacters();
    }

    fetchCharacters = async e => {
        // console.log('endpoint', this.props.endpoint);

        this.setState({ loading: true, error: null})

        try {
            const response = await fetch(this.props.endpoint);
            const data = await response.json();
            this.setState({ loading: false, data: data})
            console.log(data); 
        } catch (error) {
            this.setState({ loading: false, error: error})
        }
    }

    render() {

        const character = this.state.data;

        return (
            // {characters.map( link => (
            //     <li>{link}</li>
            // ))}  
            <tr>
                <td>{character.name}</td>
                <td>{character.height}</td>
                <td>{character.mass}</td>
                <td>{character.hair_color}</td>
                <td>{character.skin_color}</td>
                <td>{character.eye_color}</td>
                <td>{character.birth_year}</td>
                <td>{character.gender}</td>
            </tr>
            // <li></li>
            
        )
    }
}

export default CharacterDetail;